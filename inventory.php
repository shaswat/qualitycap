<?php
	session_start();
	if(!isset($_SESSION['sessionId'])){
		header("location:index.php?page=login&returnurl=".$_SERVER['REQUEST_URI']);
	}else{
?>
<div class="container">
<div class="row">
<div class="col-md-12">
<?php 
	if((isset($_SESSION["sessionId"]))&&(isset($_SESSION["role"]))&&($_SESSION["role"]=="admin")){
		$query = "SELECT * FROM cartstatus";
		$results = $con->query($query);
?>
	<table class="table">
    	<thead>
        	<tr>
            	<th>Date</th>
                <th>Cart ID</th>
                <th>Customer Name</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
        	<?php while($cart = $results->fetch_assoc()){?>
            		<tr>
                    	<td><?php echo $cart["Date"];?></td>
                        <td><a href="index.php?page=cartdetails&id=<?php echo $cart["CartId"];?>"><?php echo $cart["CartId"];?></a></td>
                        <?php 
							$user_query = "SELECT FirstName,LastName from users where UserId ='".$cart['UserId']."'";
							$user_results = $con->query($user_query);	
							$users = $user_results->fetch_assoc();
						?>
                        <td><?php echo $users['FirstName']." ".$users['LastName'];?></td>
                        <td id="<?php echo $cart["CartId"];?>"><?php echo $cart["Status"];?></td>
                        <td><input type="button" class="btn btn-secondary" value="Ship" onClick="changeStatus('<?php echo $cart["CartId"];?>')"></td>
                    </tr>
            <?php }?>
        </tbody>
    </table>
<?php
	}elseif(isset($_SESSION["sessionId"])){
		$query = "SELECT * FROM cartstatus where UserId='".$_SESSION['sessionId']."'";
		$results = $con->query($query);
?>
	<table class="table">
    	<thead>
        	<tr>
            	<th>Date</th>
                <th>Cart ID</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
        	<?php while($cart = $results->fetch_assoc()){?>
            		<?php //print_r($cart);?>
            		<tr>
                    	<td><?php echo $cart["Date"];?></td>
                        <td><a href="index.php?page=cartdetails&id=<?php echo $cart["CartId"];?>"><?php echo $cart["CartId"];?></a></td>
                        <td><?php echo $cart["Status"];?></td>
                    </tr>
            <?php }?>
        </tbody>
    </table>
<?php	
	}
?>
</div>
</div>
</div>
<?php }?>