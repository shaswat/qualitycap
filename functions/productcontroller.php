<?php
	include "../config.php";
	//adding category to database
	if($_POST['formname']=="addcategory"){
		$id = uniqid("cat_");
		$query = "insert into category (CatId, CatName) values ('".$id."','".$_POST['catName']."')";
		$con->query($query);
		if(!$con->error){
			echo "success";
		}else{
			echo $con->error;
		}
	}
	//adding supplier to database
	if($_POST['formname']=="addsupplier"){
		$id = uniqid("sup_");
		$query = "insert into supplier (SupplierId, SupplierName) values ('".$id."','".$_POST['supplierName']."')";
		$con->query($query);
		if(!$con->error){
			echo "success";
		}else{
			echo $con->error;
		}
	}
	
	//adding product to database
	if($_POST['formname']=="addproduct"){
		//inserting image into upload folder
		session_start();
		$target_dir = "../uploads/";
		$target_file = $target_dir.basename($_FILES['image']['name']);
		$fileName = "uploads/".basename($_FILES['image']['name']);
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
		//print_r($_FILES);
		move_uploaded_file($_FILES['image']['tmp_name'], $target_file);		
		//inserting information into database
		$id = uniqid("prod");
		$query = 'INSERT INTO products (ProductId, ProductName, ProductContent, Price, ImagePath, Category, Supplier) values ("'.$id.'","'.$_POST['productName'].'","'.$_POST['productDescription'].'","'.$_POST['price'].'","'.$fileName.'","'.$_POST['category'].'","'.$_POST['supplier'].'")';	
		$con->query($query);
		$url = $url.'index.php';
		header("location:".$url);
	}
	
	//adding product into a session for cart
	if($_POST['action']=='addtocart'){
		session_start();
		$ID = $_POST['id'];
		$quantity = $_POST['quantity'];
		$query = 'SELECT * FROM products where ProductId="'.$ID.'"';
		$products = $con->query($query);
		$product = $products->fetch_assoc();
		//print_r($_SESSION['addToCart']);
		if(isset($_SESSION['addToCart'])){
			$array_main=$_SESSION['addToCart'];
			if(array_key_exists($ID,$array_main)){
				$array_main[$ID]['quantity']=$array_main[$ID]['quantity']+$quantity;
				
			}else{
				$array = array(
					'id' => $ID,
					'name' => $product['ProductName'],
					'quantity' => $quantity,
					'price' => $product['Price']
				);
				$array_main[$ID] = $array;
			}
		}else{
			$array = array(
				'id' => $ID,
				'name' => $product['ProductName'],
				'quantity' => $quantity,
				'price' => $product['Price']
			);
			$array_main[$ID] = $array;
		}
		
		$_SESSION['addToCart'] = $array_main;	
		echo 'success';	
	}
	
	
	//edit quantity of product in a cart
	if($_POST['action']=="editcart"){
		session_start();
		$ID = $_POST['id'];
		$array_main = $_SESSION['addToCart'];
		$array_main[$ID]['quantity']=$_POST['value'];
		$_SESSION['addToCart'] = $array_main;
	}
	
	//checkout
	if($_POST['action']=="checkout"){
		session_start();
		if(!isset($_SESSION['sessionId'])){
			echo "loginRequired";
		}else{
			$cartItems = $_SESSION["addToCart"];
			$cartId = uniqid("cart_");
			foreach($cartItems as $cartItem){
					$orderId = uniqid("order_");
					$query = 'INSERT INTO carts(OrderId, CartId, ProductId, Price, Quantity) values ("'.$orderId.'", "'.$cartId.'", "'.$cartItem['id'].'","'.$cartItem['price'].'", "'.$cartItem['quantity'].'")';
					$con->query($query);
			}
			$user = $_SESSION['sessionId'];
			$query = "INSERT INTO cartstatus(CartId, UserId, Status, Date) values ('".$cartId."','".$user."','Pending','".date('d/m/Y')."')";
			$con->query($query);
			echo "success";
		}
	}
	
	//admin change of the status of the user's cart
	if($_POST['action']=="changeStatus"){
		$id = $_POST['id'];
		$query = "UPDATE cartstatus set status = 'Shipped' where CartId='".$id."'";
		if($con->query($query)){
			echo "success";	
		}else{
			echo "failed";	
		}
	}
	
	
	//delete product from the cart
	if($_POST["action"]=="removeCart"){
		session_start();
		$array = $_SESSION['addToCart'];
		$id=$_POST["id"];
		print_r($array);
		unset($array[$id]);
		$_SESSION['addToCart'] = $array;	
	}
	
	
	//editing the product
	if($_POST["action"]=="editProduct"){
		//$target_dir = "./uploads/";
//		$target_file = $target_dir . basename($_FILES["image"]["name"]);
//		$fileName = "uploads/".basename($_FILES["image"]["name"]);
//		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
//		move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
		$query = 'UPDATE products set ProductName="'.$_POST['name'].'",ProductContent="'.$_POST['description'].'",Price="'.$_POST['price'].'" WHERE ProductId="'.$_POST['id'].'"';
		$con->query($query);
		header("location:".$url."index.php");
	}
	
	if($_POST['action']=='edit_category'){
		$query = "UPDATE category set CatName='".$_POST["categoryName"]."'where CatId='".$_POST["id"]."'";
		$con->query($query);
		header("location:".$url."index.php");
	}
	
	if($_POST['action']=='edit_supplier'){
		$query = "UPDATE supplier set SupplierName='".$_POST["supplierName"]."'where SupplierId='".$_POST["id"]."'";
		$con->query($query);
		header("location:".$url."index.php");
	}
	
	if($_POST['action']=='deleteCategory'){
		$id = $_POST['id'];
		$query = "DELETE FROM category WHERE CatId='".$id."'";
		$con->query($query);
	}
	if($_POST['action']=='deleteSupplier'){
		$id = $_POST['id'];
		$query = "DELETE FROM supplier WHERE SupplierId='".$id."'";
		$con->query($query);
	}
	
	if($_POST['action']=='deleteProduct'){
		$id = $_POST['id'];
		$query = "DELETE FROM products WHERE ProductId='".$id."'";
		$con->query($query);
	}
	
	if($_POST['action']=='contactus'){
		$name = $_POST["name"];	
		$email = $_POST["email"];
		$subject = $_POST["subject"];
		$message = $_POST["message"];
		$subject.='<'.$email.'>';
		mail("shaswat2883@gmail.com",$subject,$message);
		header('location:'.$url.'index.php');
	}
	
	if($_POST["search"]=="Search"){
		$searchText = $_POST['searchtext'];
		header("location:".$url."index.php?search=".$searchText);	
	}
?>