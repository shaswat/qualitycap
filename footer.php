<footer class="footer">
  <div class="container">
    <p>This is a CMS block. You can insert any content (HTML, Text, Images) Here. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
  </div>
</footer>



<!--Ie Js--> 
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]--> 
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]--> 
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]--> 
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js" async : true></script>
    <![endif]--> 

<script src="https://use.fontawesome.com/6fe62b4bf4.js"></script> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js" async: true></script> 
<script src="js/validation.js" type="text/javascript"></script>
<script src="js/function.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script> 
<script src="https://raw.githubusercontent.com/scottjehl/Respond/master/dest/respond.min.js" async: true></script> 
</body>
</html>
