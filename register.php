<div class="container">
	<h2>Register</h2>
	<div class="row">
    <div class="col-md-8 login">
	<form method="post" action="#" id="register" name="register">
    <input type="hidden" name="formname" value="register" />
    <div class="alert alert-danger" id="form_error"></div>
    	<div class="form-group">
        	<label class="col-md-4 control-label" for="uname">Username</label>
            <div class="col-md-8">
            	<input type="text" class="form-control" name="uName" value="" id="uname" placeholder="Enter Username">
                <div class="alert alert-danger" id="uname_err"></div>
        	</div>
            
        </div>
        <div class="form-group">
        	<label class="col-md-4 control-label" for="firstname">First Name</label>
            <div class="col-md-8">
            	<input type="text" class="form-control" name="firstName" value="" id="firstName" placeholder="Enter First Name">
            </div>
        </div>
        <div class="form-group">
        	<label class="col-md-4 control-label" for="lastname">Last Name</label>
            <div class="col-md-8">
            	<input type="text" class="form-control" name="lastName" value="" id="lastName" placeholder="Enter Last Name">
            </div>
        </div>
        <div class="form-group">
        	<label class="col-md-4 control-label" for="email">Email</label>
            <div class="col-md-8">
            	<input type="email" class="form-control" name="email" value="" id="email" placeholder="Enter Email">
            	<div class="alert alert-danger" id="email_err"></div>
         	</div>

        </div>
        <div class="form-group">
        	<label class="col-md-4 control-label" for="password">Password</label>
            <div class="col-md-8">
            	<input type="password" class="form-control" name="password" value="" id="password" placeholder="Enter Password">
                <div class="alert alert-danger" id="pwd_err"></div>
            </div>
        </div>
        <div class="form-group">
        	<label class="col-md-4 control-label" for="re-password">Re-type Password</label>
            <div class="col-md-8">
            	<input type="password" class="form-control" name="re-password" value="" id="re-password" placeholder="Re-enter Password">
               	<div class="alert alert-danger" id="conpwd_err"></div>
           	</div>
        </div>
        <div class="form-group">
        	<label class="col-md-4" for="phone">Phone</label>
            <div class="col-md-8">
            	<input type="tel" class="form-control" name="phone" id="phone" value="" placeholder="Enter Phone Number">
                <div class="alert alert-danger" id="phone_err"></div>
        	</div>
        </div>
        <div class="form-group">
        	<label class="col-md-4 control-label" for="Address">Address</label>
            <div class="col-md-8">
            <textarea class="form-control" name="address" id="address" rows="3" placeholder="Enter Phone Number"></textarea>
            </div>
        </div>
        <?php if((isset($_SESSION['role']))&&($_SESSION['role']=="admin")){?>
        <div class="form-group">
        	<label class="col-md-4 control-label" for="Address">Role</label>
            <div class="col-md-8">
            	<select name="role" id="role">
                	<option value="admin">Administrator</option>
                    <option value="user">User</option>
                </select>
            </div>
        </div>
        <?php }?>
        <button type="button" onClick="registerValidation()" class="btn btn-primary" style="float:right;">Register</button>
    </form>
    </div>
    </div>
</div>