<div class="container">
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Index</th>
        <th>Product Name</th>
        <th style="width:179px;">Quantity</th>
        <th>Price</th>
        <th>Total</th>
      </tr>
    </thead>
    <tbody>
      <?php
				$purchases = $_SESSION['addToCart'];
				$i=1;
				foreach($purchases as $purchase){
			?>
      <tr>
        <td><?php echo $i++;?></td>
        <td><?php echo $purchase['name'];?></td>
        <td><input type="number" onChange="editCart('<?php echo $purchase['id']?>')" class="form-control" id="quantity_<?php echo $purchase['id']?>" value="<?php echo $purchase['quantity'];?>"></td>
        <td id="price_<?php echo $purchase['id']?>"><?php echo $purchase['price'];?></td>
        <?php $total = $purchase['price']*$purchase['quantity'];?>
        <td id="total_<?php echo $purchase['id']?>"><?php echo $total;?></td>
        <td style="width:20px;"><input type="button" class="btn btn-danger" onClick="removeCart('<?php echo $purchase['id']?>')" value="Delete"></td>
      </tr>
      <?php
					$grandtotal = $grandtotal + $total;
				}
				$taxamount = 0.12*$grandtotal;
				$nettotal = $grandtotal + $taxamount;
			?>
      <tr>
        <th colspan="3" style="text-align:right;"><strong>Grand Total</strong></th>
        <th colspan="2" style="text-align:right;"><strong id="grandTotal"><?php echo $grandtotal;?></strong></th>
      </tr>
      <tr>
        <th colspan="3" style="text-align:right;"><strong>12% taxation</strong></th>
        <th colspan="2" style="text-align:right;" ><strong id="tax"><?php echo $taxamount;?></strong></th>
      </tr>
      <tr>
        <th colspan="3" style="text-align:right;"><strong>Net Total</strong></th>
        <th colspan="2" style="text-align:right;"><strong id="netTotal"><?php echo $nettotal;?></strong></th>
      </tr>
      <tr>
        <th colspan="5" style="text-align:right;"><input type="button" onClick="checkout()" value="Checkout" class="btn btn-success">
        </th>
      </tr>
    </tbody>
  </table>
</div>
