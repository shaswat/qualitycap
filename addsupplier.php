<?php
	session_start();
	if((isset($_SESSION['role']))||($_SESSION['role']=='admin')) {
?>
<div class="container">
	<div class="row">
    <div class="col-md-8">
    	<h2>Create New Supplier</h2>
    	<form method="post" action="#" id="addsupplier" name="addsupplier">
        	<input type="hidden" id="formname" name="formname" value="addsupplier">
            <div class="alert alert-danger" id="form_error"></div>
        	<div class="form-group">
                <label class="control-label" for="uname">Supplier Name</label>
                    <input type="text" class="form-control" name="supplierName" value="" id="supplierName" placeholder="Enter Supplier">
                    <div class="alert alert-danger" id="cat_err"></div>
        	</div>
            <input type="button" style="float:right;" class="btn btn-primary" value="Add Supplier" onClick="addSupplier()">
        </form>
     	</div>
        <div class="col-md-4">
        <h3>Suppliers</h3>
        <ul>
        <?php 
			$query = "select * from supplier";
			$supplier = $con->query($query);
			while($row = $supplier->fetch_assoc()) {?>
        	<li>
            	<strong><?php echo $row['SupplierName'];?></strong>
                <a href="?page=edit&type=supplier&id=<?php echo $row['SupplierId'];?>" >Edit</a>
 				<button onclick="deletesupplier('<?php echo $row['SupplierId'];?>')" >Delete</button>
            </li>
            <?php }?>
        </ul>
        </div>
    </div>
</div>
<?php }else{
		header("location:index.php?page=login&returnurl=".$_SERVER['REQUEST_URI']);
}