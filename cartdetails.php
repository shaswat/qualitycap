<?php if(isset($_GET["id"])){?>
<div class="container">
	<div class="row">
    	<div class="col-md-12">
        	<table class="table table-striped">
        	<thead>
            	<tr>
                	<th>Index</th>
                	<th>Product Name</th>
                    <th style="width:179px;">Quantity</th>
                    <th>Price</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
            <?php
				$query = "SELECT * FROM carts where CartId='".$_GET['id']."'";
				$items = $con->query($query);
				$i=0;
				while($purchase = $items->fetch_assoc()){
			?>
            	<tr>
                	<td><?php echo $i++;?></td>
                    <?php
						$product_query = "SELECT ProductName FROM products where ProductId='".$purchase['ProductId']."'";
						$product_result = $con->query($product_query);
						$product = $product_result->fetch_assoc();
					?>
                	<td><?php echo $product['ProductName'];?></td>
                    <td><?php echo $purchase['Quantity'];?></td>
                    <td><?php echo $purchase['Price'];?></td>
                    <?php $total = $purchase['Price']*$purchase['Quantity'];?>
                    <td><?php echo $total;?></td>
                </tr>
            <?php
					$grandtotal = $grandtotal + $total;
				}
				$taxamount = 0.12*$grandtotal;
				$nettotal = $grandtotal + $taxamount;
			?>
            <tr>
            	<th colspan="3" style="text-align:right;"><strong>Grand Total</strong></th>
                <th colspan="2" style="text-align:right;"><strong id="grandTotal"><?php echo $grandtotal;?></strong></th>
            </tr>
            <tr>
            	<th colspan="3" style="text-align:right;"><strong>12% taxation</strong></th>
                <th colspan="2" style="text-align:right;" ><strong id="tax"><?php echo $taxamount;?></strong></th>
            </tr>
            <tr>
            	<th colspan="3" style="text-align:right;"><strong>Net Total</strong></th>
                <th colspan="2" style="text-align:right;"><strong id="netTotal"><?php echo $nettotal;?></strong></th>
            </tr>
            </tbody>
        </table>
        </div>
    </div>
</div>
<?php }?>