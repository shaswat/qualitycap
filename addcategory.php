<?php
	session_start();
	if((isset($_SESSION['role']))||($_SESSION['role']=='admin')) {
?>

<div class="content">
  <div class="container login">
    <h2>Create New Category</h2>
    <div class="row">
      <div class="col-md-4">
        <form method="post" action="#" id="addcategory" name="addcategory">
          <input type="hidden" id="formname" name="formname" value="addcategory">
          <div class="alert alert-danger" id="form_error"></div>
          <div class="form-group">
            <label class=" control-label" for="uname">Category Name</label>
            <input type="text" class="form-control" name="catName" value="" id="catName" placeholder="Enter Category">
            <div class="alert alert-danger" id="cat_err"></div>
          </div>
          <input type="button" style="float:right;" class="btn btn-primary" value="Add Category" onClick="addCategory()">
        </form>
      </div>
      <div class="col-md-offset-2 col-md-4">
        <h4>Categories</h4>
        <ul>
          <?php 
			$query = "select * from category";
			$category = $con->query($query);
			while($row = $category->fetch_assoc()) {?>
          <li> <strong><?php echo $row['CatName'];?></strong> <br />
            <a href="?page=edit&type=category&id=<?php echo $row['CatId'];?>" >Edit</a> | <a onClick="deletecategory('<?php echo $row['CatId'];?>')">Delete</a> </li>
          <?php }?>
        </ul>
      </div>
    </div>
  </div>
</div>
<?php }else{
		header("location:index.php?page=login&returnurl=".$_SERVER['REQUEST_URI']);
}


