<?php if(isset($_SESSION['sessionId'])){ ?>
<?php
	$query = "SELECT * FROM users WHERE UserId='".$_SESSION['sessionId']."'";
	$user_results = $con->query($query);
	$users = $user_results->fetch_assoc();
?>
<div class="container">
	<div class="row">
    	<div class="col-md-8">
    	<form method="post" action="functions/usercontroller.php" name="profile" id="profile">
        	<input type="hidden" name="action" value="profileEdit">
        	<div class="form-group row">
                <label class="col-xs-2 col-form-label" for="username">Username</label>
                <div class="col-xs-10">
                	<input type="text" name="uname" class="form-control" id="uname" value="<?php echo $users['UserName'];?>" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xs-2 col-form-label" for="username">First Name</label>
                <div class="col-xs-10">
                	<input type="text" name="fname" class="form-control" id="fname" value="<?php echo $users['FirstName'];?>" />
             	</div>
            </div>
            <div class="form-group row">
                <label class="col-xs-2 col-form-label" for="username">Last Name</label>
                <div class="col-xs-10">
                <input type="text" name="lname" class="form-control" id="lname" value="<?php echo $users['LastName'];?>" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xs-2 col-form-label" for="username">Email</label>
                <div class="col-xs-10">
                	<input type="text" name="email" class="form-control" id="email" value="<?php echo $users['Email'];?>" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xs-2 col-form-label" for="username">Phone</label>
                <div class="col-xs-10">
                	<input type="tel" name="phone" class="form-control" id="phone" value="<?php echo $users['Phone'];?>" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xs-2 col-form-label" for="username">Address</label>
                <div class="col-xs-10">
                	<textarea class="form-control" id="address" name="address"><?php echo $users['Address'];?></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xs-2 col-form-label" for="password">Password</label>
                <div class="col-xs-10">
                	<input type="text" name="password" class="form-control" id="password" value="<?php echo $users['Password'];?>" />
                </div>
            </div>
            <input type="submit" name="action" value="Save Profile" class="btn btn-primary" >
        </form>
        </div>
    </div>
</div>
<?php }else{ ?>
	<?php header("location:index.php?page=login");?>
<?php }?>