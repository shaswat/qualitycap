// JavaScript Document

function addProduct(){
	var name = document.getElementById("productName").value;
	var price = document.getElementById("price").value;
	var errorCount = 0;
	if(name==""){
		document.getElementById("pname_err").innerHTML = "Fill the Product Name";
		document.getElementById("pname_err").style.display = "block";
		errorCount++;
	}
	if(price==""){
		document.getElementById("price_err").innerHTML = "Fill the Product Price";
		document.getElementById("price_err").style.display = "block";
		errorCount++;
	}
	if(errorCount==0){
		data = $('#addproduct').serialize();
		
		document.getElementById("addproduct").submit();
		//document.location.href = "index.php";
	}
}

function addCategory(){
	var name = document.getElementById("catName").value;
	if(name!=""){
		var data = $('#addcategory').serialize();
			$.post("functions/productcontroller.php",data , function (response) {
				// Response div goes here.
				if(response=="success"){
						window.location.href = "index.php";
				}else{
					document.getElementById('form_error').innerHTML=response;
					document.getElementById('form_error').style.display = "block";
				}
			});
	}else{
			document.getElementById('cat_err').innerHTML="Enter the category name";
			document.getElementById('cat_err').style.display = "block";	
	}
}

function addSupplier(){
	var name = document.getElementById("supplierName").value;
	if(name!=""){
		var data = $('#addsupplier').serialize();
			$.post("functions/productcontroller.php",data , function (response) {
				// Response div goes here.
				if(response=="success"){
						window.location.href = "index.php";
				}else{
					document.getElementById('form_error').innerHTML=response;
					document.getElementById('form_error').style.display = "block";
				}
			});
	}else{
			document.getElementById('cat_err').innerHTML="Enter the supplier name";
			document.getElementById('cat_err').style.display = "block";	
	}
}

function addToCart(Id){
	var quantity = document.getElementById(Id).value;
	var data = {'action':'addtocart','id':Id, 'quantity': quantity};
	$.post("functions/productcontroller.php",data , function (response) {
				// Response div goes here.
				if(response=="success"){
						document.getElementById('form_success').innerHTML="Successfully added to cart";
						document.getElementById('form_success').style.display = "block";
						document.getElementById('dynamic-cart').style.display = "block";
				}else{
					document.getElementById('form_error').innerHTML=response;
					document.getElementById('form_error').style.display = "block";
				}
	});
}

function editCart(ID){
	var value = document.getElementById("quantity_"+ID).value;
	var price = document.getElementById("price_"+ID).innerHTML;
	var grandTotal = document.getElementById("grandTotal").innerHTML;
	var tax = document.getElementById("tax").innerHTML;
	var netTotal = document.getElementById("netTotal").innerHTML;
	var total = document.getElementById("total_"+ID).innerHTML;
	var nebTotal = value*price;
	var difference = total-nebTotal;
	grandTotal = grandTotal-difference;
	tax = 0.12*grandTotal;
	netTotal = grandTotal+tax;
	data = {'action':'editcart', 'value':value, 'id':ID};
	$.post("functions/productcontroller.php",data , function (response) {
		document.getElementById("total_"+ID).innerHTML = nebTotal;
		document.getElementById("grandTotal").innerHTML = grandTotal;
		document.getElementById("tax").innerHTML = tax;
		document.getElementById("netTotal").innerHTML = netTotal;
	});
}

function checkout(){
	data = {'action':'checkout'};
	$.post("functions/productcontroller.php", data , function (response) {
		if(response=="loginRequired"){
			window.location.href = "index.php?page=login&returnurl=cart";
		}else if(response=="success"){
			window.location.href = "index.php?page=inventory";	
		}
	});
}

function changeStatus(id){
	data = {'action':'changeStatus', 'id':id};
	$.post("functions/productcontroller.php", data , function (response) {
		if(response=="success"){
			document.getElementById(id).innerHTML="Shipped";
		}
	});
}

function removeCart(ID){
	var grandTotal = document.getElementById("grandTotal").innerHTML;
	var tax = document.getElementById("tax").innerHTML;
	var netTotal = document.getElementById("netTotal").innerHTML;
	var total = document.getElementById("total_"+ID).innerHTML;
	data = {'action':'removeCart', 'id':ID};
	$.post("functions/productcontroller.php", data , function (response) {
		window.location.href = "index.php?page=cart";
	});
	
}


function deletecategory(ID){
	data = {'action':'deleteCategory', 'id':ID};
	$.post("functions/productcontroller.php", data , function (response) {
		window.location.href = "index.php?page=addcategory";
	});
}
function deletesupplier(ID){	
	data = {'action':'deleteSupplier', 'id':ID};
	$.post("functions/productcontroller.php", data , function (response) {
		window.location.href = "index.php?page=addsupplier";
	});
}

function deleteproduct(ID){
	data = {'action':'deleteProduct', 'id':ID};
	$.post("functions/productcontroller.php", data , function (response) {
		window.location.href = "index.php";
	});
}