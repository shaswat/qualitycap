// JavaScript Document
function registerValidation(){
	var uname = document.getElementById("uname").value;
	var email = document.getElementById("email").value;
	var phone = document.getElementById("phone").value;
	var pwd = document.getElementById("password").value;
	var conpwd = document.getElementById("re-password").value;
	var errorCount = 0;
	var errors = document.getElementsByClassName("alert");
	for(var i=0; i<errors.length; i++){
		errors[i].style.display = "none";	
	}
	if(isEmpty(uname)){
		document.getElementById("uname_err").innerHTML = "Fill the username";
		document.getElementById("uname_err").style.display = "block";
		errorCount++;
	}
	if(isEmpty(email)){
		document.getElementById("email_err").innerHTML = "Fill the email";	
		document.getElementById("email_err").style.display = "block";
		errorCount++;
	}
	if(!isValidEmail(email)){
		document.getElementById("email_err").innerHTML = "Enter a valid email address";	
		document.getElementById("email_err").style.display = "block";
		errorCount++;
	}
	if(isEmpty(pwd)){
		document.getElementById("pwd_err").innerHTML = "Fill the password";	
		document.getElementById("pwd_err").style.display = "block";
		errorCount++;
	}
	if(isEmpty(phone)){
		document.getElementById("phone_err").innerHTML = "Fill the phone number";	
		document.getElementById("phone_err").style.display = "block";
		errorCount++;
	}
	if(isEmpty(conpwd)){
		document.getElementById("conpwd_err").innerHTML = "Fill the confirm password";
		document.getElementById("conpwd_err").style.display = "block";
		errorCount++;
	}
	if((pwd.length<=7)||(!hasUpperCase(pwd))||(!hasNumber(pwd))){
		document.getElementById("pwd_err").innerHTML = "Password must be atleast 7 digits with atleast 1 uppercare alphabet and one number";
		document.getElementById("pwd_err").style.display = "block";
		errorCount++;
	}
	if(pwd!=conpwd){
		document.getElementById("pwd_err").innerHTML = "Password Mismatch";
		document.getElementById("pwd_err").style.display = "block";
		errorCount++;
	}
	if(errorCount==0){
		//document.getElementById("register").submit();
		var data = $('#register').serialize();
		$.post("functions/usercontroller.php",data , function (response) {
            // Response div goes here.
            if(response=="success"){
					window.location.href = "?page=success";
					
			}else{
				document.getElementById('form_error').innerHTML=response;
				document.getElementById('form_error').style.display = "block";
			}
        });
	
		//var jsonData = $.ajax({
//                url: "functions/usercontroller.php",
//                data: $('#register').serialize(),
//                dataType: "json",
//                async: false
//            }).responseText;
	}
}

function loginValidation(){
	var uname = document.getElementById("uname").value;
	var pwd = document.getElementById("password").value;
	var returnUrl = document.getElementById("returnurl").value;
	var errorCount = 0;
	var errors = document.getElementsByClassName("alert");
	for(var i=0; i<errors.length; i++){
		errors[i].style.display = "none";	
	}
	if(isEmpty(uname)){
		document.getElementById("uname_err").innerHTML = "Fill the username";
		document.getElementById("uname_err").style.display = "block";
		errorCount++;
	}
	if(isEmpty(pwd)){
		document.getElementById("pwd_err").innerHTML = "Fill the password";	
		document.getElementById("pwd_err").style.display = "block";
		errorCount++;
	}
	if(errorCount==0){
		var data = $('#login').serialize();
		$.post("functions/usercontroller.php",data , function (response) {
            // Response div goes here.
            if(response=="success"){
				if(isEmpty(returnUrl)){
					window.location.href = "index.php";
				}
				else{
					window.location.href = "index.php?page="+returnUrl;
				}
			}else{
				document.getElementById('form_error').innerHTML=response;
				document.getElementById('form_error').style.display = "block";
			}
        });
	}
	
}

function logout(){
		$.post("functions/usercontroller.php?action=logout", function (response) {
            // Response div goes here.
            if(response=="success"){
				if(isEmpty(returnUrl)){
					window.location.href = "index.php";
				}
				else{
					window.location.href = "index.php?page="+returnUrl;
				}
			}else{
				document.getElementById('form_error').innerHTML=response;
				document.getElementById('form_error').style.display = "block";
			}
        });
}

function isEmpty(value){
  return (value == null || value.length === 0);
}

function hasUpperCase(str) {
    return (/[A-Z]/.test(str));
}

function hasNumber(str) {
	return (/[0-9]/.test(str));
}

function isValidEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
