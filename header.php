<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Amanti</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link rel="icon" href="favicon.png" type="image/x-icon" />
<!--global css-->
<link href="css/global.css" rel="stylesheet" type="text/css" />
<!--bootstrap css-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>
<body>
<?php 
	//$url = "http://" . $_SERVER['SERVER_NAME']; //. $_SERVER['REQUEST_URI']; 
	include "config.php";
	session_start();
?>
<!--header top-->
<nav class="header_top">
  <div class="container">
    <div class="row">
      <ul class="links">
        <li><i class="fa fa-phone"></i> +61 1234 567 890</li>
        <li><a href="mailto:info@xyz.com"><i class="fa fa-envelope"></i> info@xyz.com</a></li>
      </ul>
      <div class="pull-right">
        <ul class="links">
        	<?php 
				if(!isset($_SESSION['sessionId'])){
			?>
          		<li><a href="<?php echo $url;?>?page=login">Login</a></li>
          		<li><a href="<?php echo $url;?>?page=register">Register</a></li>
            <?php 
				}else{
					if((isset($_SESSION['role']))&&($_SESSION['role']=='admin')){
			?>
            			<li><a href="<?php echo $url;?>?page=register">Register</a></li>
            <?php 	}?>
            	<li><a href="index.php?page=profile">Profile</a></li>
            	<li><a href="logout.php">logout</a></li>
            <?php }?>
          <li><a href="#">Checkout</a></li>
          <?php if(isset($_SESSION['addToCart'])){ $style = "inline-block"; }else{ $style = "none"; } ?>
          <li id="dynamic-cart" style="display:<?php echo $style;?>;"><a href="index.php?page=cart"><i class="fa fa-briefcase"></i> Cart</a></li>
        </ul>
      </div>
    </div>
  </div>
</nav>

<!--header bottom-->

<header class="header">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-md-6"> <a href="index.php" class="logo"><img src="images/logoCap.gif"></a></div>
      <div class=" col-md-offset-3 col-sm-3 col-md-3" style="margin-top:50px;">
      <form action="functions/productcontroller.php" method="post" name="search">
        <div class="input-group">
          <input type="text" name="searchtext" class="form-control" placeholder="Search for...">
          <span class="input-group-btn">
          <input class="btn btn-default" name="search"  type="submit" value="Search">
          </span>
        </div>
      </form>
      </div>
    </div>
  </div>
</header>
<div class="container">
  <nav class="navbar navbar-default">
    <div class="container-fluid"> 
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      </div>
      
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li><a href="index.php">Home <span class="sr-only">(current)</span></a></li>
           <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Category <span class="caret"></span></a>
          <ul class="dropdown-menu">
           			<?php 
						$query = "select * from category";
						$category = $con->query($query);
						while($row = $category->fetch_assoc()) {?>
								<li><a href="index.php?category=<?php echo $row['CatId']?>"><?php echo $row['CatName'];?></a></li>
					<?php
						}
					?>
          </ul>
          <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Supplier <span class="caret"></span></a>
          <ul class="dropdown-menu">
           			<?php 
						$query = "select * from supplier";
						$supplier = $con->query($query);
						while($row = $supplier->fetch_assoc()) {?>
								<li><a href="index.php?supplier=<?php echo $row['SupplierId']?>"><?php echo $row['SupplierName'];?></a></li>
					<?php
						}
					?>
          </ul>
          <?php 
		  	if((isset($_SESSION['sessionId']))&&(isset($_SESSION['role']))&&($_SESSION['role']=="admin")){
		  ?>
          	<li><a href="index.php?page=users">Users</a></li>
          <?php } ?>
          <?php 
		  	if(isset($_SESSION['sessionId'])){
		  ?>
          	<li><a href="index.php?page=inventory">Inventory</a></li>
          <?php } ?>
          <li><a href="index.php?page=contactus">Contact Us</a></li>
        </ul>
      </div>
    </div>
  </nav>
</div>