<?php
if(!isset($_GET['category'])){?>

<div class="container">
  <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
    </ol>
    
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active"> <img src="images/banner.jpg" alt=""> </div>
      <div class="item"> <img src="images/banner1.jpg" alt=""> </div>
    </div>
    
    <!-- Left and right controls --> 
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
</div>
<?php
}
?>
<div class="container product">
  <h2>Latest Products</h2>
  <?php if((isset($_SESSION['role']))&&($_SESSION['role']=='admin')){?>
  <a href="?page=addproduct" class="btn btn-primary">Add Product</a> <a href="?page=addcategory" class="btn btn-success">Add Category</a> <a href="?page=addsupplier" class="btn btn-danger">Add Supplier</a><br />
  <br />
  <?php }?>
  <div class="row">
    <div id="form_success"></div>
    <?php
	if(isset($_GET['category'])){
		$query = 'select * from products where Category="'.$_GET['category'].'"';
	}elseif(isset($_GET['supplier'])){
		$query = 'select * from products where Supplier="'.$_GET['supplier'].'"';
	}elseif(isset($_GET['search'])){
		$query = 'select * from products where ProductName LIKE "%'.$_GET['search'].'%"';
	}else{
		$query = "select * from products";
	}
	$products = $con->query($query);
	$i=0;
	while($product = $products->fetch_assoc()) {
?>
    <div class="col-sm-3 col-xs-12">
      <div class="product_image"><img class="img-responsive" title="<?php echo $product['ProductName'];?>" alt="<?php echo $product['ProductName'];?> " src="<?php echo $product['ImagePath'];?>"></div>
      <h4><a href="?page=productdetail&id=<?php echo $product['ProductId'];?>"><?php echo $product['ProductName'];?></a></h4>
      <p> <span class="price-new">$<?php echo $product['Price'];?></span> <span class="price-tax">Ex Tax: $<?php echo 0.12*$product['Price'];?></span> </p>
      <div class="row">
        <div class="col-md-8">
          <form>
            <div class="input-group">
              <input type="number" class="form-control" id="<?php echo $product['ProductId'];?>" value="1">
              <span class="input-group-btn">
              <button onclick="addToCart('<?php echo $product['ProductId'];?>')" type="button" class="btn btn-primary"><span>Add to Cart</span></button>
              </span> </div>
          </form>
        </div>
      </div>
      <?php if((isset($_SESSION['role']))&&($_SESSION['role']=='admin')){?>
      <a href="index.php?page=productedit&id=<?php echo $product['ProductId'];?>">Edit</a> | <a onclick="deleteproduct('<?php echo $product['ProductId'];?>')">Delete</a><br /><br />
      <?php }?>
    </div>
    <?php
		$i++;
		if($i%4==0){
			echo "</div><div class='row'>";
		}
	}
	?>
  </div>
</div>
