<div class="container">
	<h2>Register</h2>
	<div class="row">
    <div class="col-md-8 login">
	<form method="post" action="#" name="login" id="login">
    	<input type="hidden" name="formname" value="login" />
        <div class="alert alert-danger" id="form_error"></div>
        <?php if(isset($_GET['returnurl'])){?>
        	<input type="hidden" id="returnurl" value="<?php echo $_GET['returnurl'];?>" />
        <?php }?>
    	<div class="form-group">
        	<label class="col-md-4 control-label" for="uname">Username</label>
            <div class="col-md-8">
            	<input type="text" class="form-control" name="uName" value="" id="uname" placeholder="Enter Username">
                <div class="alert alert-danger" id="uname_err"></div>
        	</div>
        </div>
        <div class="form-group">
        	<label class="col-md-4 control-label" for="password">Password</label>
            <div class="col-md-8">
            	<input type="password" class="form-control" name="password" value="" id="password" placeholder="Enter Password">
                <div class="alert alert-danger" id="pwd_err"></div>
            </div>
        </div>
        <input type="hidden" id="returnurl" name="returnurl" value="<?php echo $_GET['returnurl'];?>" />
        <button type="button" onClick="loginValidation()" class="btn btn-secondary" style="float:right;">Login</button>
    </form>
    </div>
    </div>
</div>