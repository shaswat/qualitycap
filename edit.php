<?php
	session_start();
	if((isset($_SESSION['role']))||($_SESSION['role']=='admin')) {
		$type=$_GET['type'];
		if($type=="category"){
			$dbtype='Cat';
		}else{
			$dbtype='Supplier';
		}
		$query = "SELECT * FROM ".$type." where ".$dbtype."Id='".$_GET["id"]."'";
		$result = $con->query($query);
		$rows = $result->fetch_assoc();
?>
<div class="container">
	<div class="row">
    <div class="col-md-8">
    	<h2>Edit <?php echo $type;?></h2>
    	<form method="post" action="functions/productcontroller.php" id="edit" name="edit">
        	<input type="hidden" id="formname" name="action" value="edit_<?php echo $type;?>">
            <input type="hidden" name="id" value="<?php echo $rows[$dbtype.'Id']?>">
            <div class="alert alert-danger" id="form_error"></div>
        	<div class="form-group">
                <label class=" control-label" for="uname"><?php echo $type;?> Name</label>
                
                    <input type="text" class="form-control" value="<?php echo $rows[$dbtype.'Name'];?>" name="<?php echo $type;?>Name" value="" id="<?php echo $type;?>Name" >
                    <div class="alert alert-danger" id="<?php echo $type;?>_err"></div>
        	</div>
            <input type="submit" style="float:right;" class="btn btn-primary" value="Edit <?php echo $type;?>" >
        </form>
            </div>
    </div>
</div><?php }else{
		header("location:index.php?page=login&returnurl=".$_SERVER['REQUEST_URI']);
}