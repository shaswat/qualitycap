<div class="container">
	<div class="row">
    	<div class="col-md-12">
        	<?php 
				$query = "SELECT * FROM users";
				$users = $con->query($query);
			?>
            <table class="table">
            	<thead>
                	<tr>
                    	<th>UserId</th>
                        <th>UserName</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Address</th>
                    </tr>
                </thead>
                <tbody>
                	<?php while($user = $users->fetch_assoc()){?>
                    	<tr>
                        	<td><?php echo $user["UserId"];?></td>
                            <td><?php echo $user["UserName"];?></td>
                            <td><?php echo $user["FirstName"];?></td>
                            <td><?php echo $user["LastName"];?></td>
                            <td><?php echo $user["Email"];?></td>
                            <td><?php echo $user["Phone"];?></td>
                            <td><?php echo $user["Address"];?></td>
                        </tr>
                    <?php }?>
                </tbody>
            </table>
        </div>
    </div>
</div>