<?php
	session_start();
	if((isset($_SESSION['role']))||($_SESSION['role']=='admin')) {
?>

<div class="content">
  <div class="container login">
    <div class="row">
      <div class="col-md-6">
        <h2>Add Product</h2>
        <br />
        <form method="post" action="functions/productcontroller.php" id="addproduct" name="addproduct" enctype="multipart/form-data">
          <input type="hidden" id="formname" name="formname" value="addproduct">
          <div class="alert alert-danger" id="form_error"></div>
          <div class="form-group">
            <div class="row">
              <label class="col-md-4 control-label" for="uname">Product Name</label>
              <div class="col-md-8">
                <input type="text" class="form-control" name="productName" value="" id="productName" placeholder="Enter Name">
                <div class="alert alert-danger" id="pname_err"></div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <label class="col-md-4 control-label" for="uname">Product Description</label>
              <div class="col-md-8">
                <input type="text" class="form-control" name="productDescription" value="" id="productDescription" placeholder="Enter Description">
                <div class="alert alert-danger" id="description_err"></div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <label class="col-md-4 control-label" for="uname">Category</label>
              <div class="col-md-8">
                <select class="form-control" name="category" id="category">
                  <?php 
						$query = "select * from category";
						$category = $con->query($query);
						while($row = $category->fetch_assoc()) {?>
                  <option value="<?php echo $row['CatId']?>"><?php echo $row['CatName'];?></option>
                  <?php
						}
					?>
                </select>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <label class="col-md-4 control-label" for="supplier">Supplier</label>
              <div class="col-md-8">
                <select class="form-control" name="supplier" id="supplier">
                  <?php 
						$query = "select * from supplier";
						$supplier = $con->query($query);
						while($row = $supplier->fetch_assoc()) {?>
                  <option value="<?php echo $row['SupplierId']?>"><?php echo $row['SupplierName'];?></option>
                  <?php
						}
					?>
                </select>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <label class="col-md-4 control-label" for="uname">Image</label>
              <div class="col-md-8">
                <input type="file" class="form-control" name="image" id="image" >
                <div class="alert alert-danger" id="image_err"></div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <label class="col-md-4 control-label" for="price">Price</label>
              <div class="col-md-8">
                <input type="text" class="form-control" name="price" value="" id="price" placeholder="Enter Price">
                <div class="alert alert-danger" id="price_err"></div>
              </div>
            </div>
          </div>
          <input class="btn btn-danger pull-right" type="button" value="Add Product" onClick="addProduct()">
        </form>
      </div>
    </div>
  </div>
</div>
<?php }else{
		header("location:index.php?page=login&returnurl=".$_SERVER['REQUEST_URI']);
}
	






