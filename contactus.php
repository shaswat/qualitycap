<div class="container">
	<div class="row">
    	<div class="col-md-8">
        <h2>Contact Us:</h2>
        	<form method="post" action="functions/productcontroller.php" name="contact" id="contact">
            	<input type="hidden" name="action" value="contactus">
            	<div class="form-group ">
            	<label for="name">Name</label>
                <input type="text" class="form-control" name="name" placeholder="Enter Your Name" />
                </div>
                <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" name="email" placeholder="Enter Your Email" />
                </div>
                <div class="form-group ">
                <label for="subject">Subject</label>
                <input type="text" class="form-control" name="subject" placeholder="Enter Your Subject" />
                </div>
                <div class="form-group ">
                <label for="Message">Message</label>
                <textarea class="form-control" name="message" placeholder="Enter Your Message"></textarea>
                </div>
                <input type="submit" class="btn btn-primary" value="Send" >
            </form>
        </div>
        <div class="col-md-4">
        	<strong>Contact Us:</strong>
            <address>Unitec Institute of Technology<br>Carrington Road, Mount Albert<br>Auckland</address>
            <span><strong>Phone us : </strong></span><span>+64-22-041-4939</span><br>
            <span><strong>Email us : </strong></span><span>shaswat2883@gmail.com</span>
        </div>
    </div>
</div>